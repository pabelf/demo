package com.example.doyouevenlist.ui.list_activity.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.doyouevenlist.R
import com.example.doyouevenlist.data.models.Article
import kotlinx.android.synthetic.main.article_view.view.*

class ArticleAdapter(private val articles: List<Article>) : RecyclerView.Adapter<ArticleAdapter.ArticleViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder =
        ArticleViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.article_view, parent, false))

    override fun getItemCount(): Int = articles.size

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        holder.bind(articles[position])
    }

    class ArticleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(article: Article) {
            itemView.apply {

                article.source?.name?.let {
                    article_source.text = it
                }

                article.urlToImage?.let {
                    Glide.with(article_image_view.context)
                        .load(article.urlToImage)
                        .into(article_image_view)
                    //Todo add placeholder loading with glide
                }

                article.title?.let {
                    article_title.text = it
                }

                article.description?.let {
                    article_description.text = it
                }
            }
        }
    }
}