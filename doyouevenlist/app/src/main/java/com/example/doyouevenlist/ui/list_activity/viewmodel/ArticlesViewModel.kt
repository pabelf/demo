package com.example.doyouevenlist.ui.list_activity.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.doyouevenlist.data.api.NewsRepository
import com.example.doyouevenlist.data.models.Response
import kotlinx.coroutines.Dispatchers

class ArticlesViewModel(private val newsRepository: NewsRepository) : ViewModel() {

    fun getArticles() = liveData(Dispatchers.IO) {
        emit(Response.Loading)

        try {
            emit(Response.Success(data = newsRepository.getArticles()))
        } catch (e: Exception) {
            emit(Response.Error(e))
        }
    }
}