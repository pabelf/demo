package com.example.doyouevenlist.data.models

data class Article (
    val source : Source?,
    val urlToImage : String?,
    val title : String?,
    val description : String?
)