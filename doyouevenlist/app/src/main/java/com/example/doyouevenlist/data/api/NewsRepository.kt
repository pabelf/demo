package com.example.doyouevenlist.data.api

import com.example.doyouevenlist.data.models.ArticleResponse

open class NewsRepository(val newsApi: NewsApi) {
    open suspend fun getArticles(): ArticleResponse {
        //Todo consider passing api key in header if time allows
        return newsApi.getArticles(
            "bitcoin",
            "2020-07-22", //Note: had to use 07-22 instead of 06-15 : "You are trying to request results too far in the past. Your plan permits you to request articles as far back as 2020-07-15, but you have requested 2020-06-15. You may need to upgrade to a paid plan."
            "publishedAt",
            "placeApiKeyHere"
        )
    }
}