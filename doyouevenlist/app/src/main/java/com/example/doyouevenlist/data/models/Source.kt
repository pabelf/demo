package com.example.doyouevenlist.data.models

data class Source (
    val id : String?,
    val name : String?
)