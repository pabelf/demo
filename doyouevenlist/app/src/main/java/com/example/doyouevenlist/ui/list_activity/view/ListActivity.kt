package com.example.doyouevenlist.ui.list_activity.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.doyouevenlist.R
import com.example.doyouevenlist.data.models.Article
import com.example.doyouevenlist.data.models.Response
import com.example.doyouevenlist.ui.list_activity.adapter.ArticleAdapter
import com.example.doyouevenlist.ui.list_activity.viewmodel.ArticlesViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject

class ListActivity : AppCompatActivity() {

    private val articlesViewModel: ArticlesViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initUi()
        initViewModelObservers()
    }

    private fun initUi() {
        val horizontalLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        article_recycler_view.setHasFixedSize(true)
        article_recycler_view.layoutManager = horizontalLayoutManager
    }

    private fun initViewModelObservers() {
        articlesViewModel.getArticles().observe(this, Observer {
            it.let {response ->
                when(response) {
                    is Response.Loading -> showLoading()
                    is Response.Success -> showSuccess(response.data.articles)
                    is Response.Error -> showError()
                }
            }
        })
    }

    private fun showLoading() {
        progress_circular.visibility = View.VISIBLE
    }

    private fun showSuccess(articles : List<Article>) {
        progress_circular.visibility = View.GONE
        article_recycler_view.visibility = View.VISIBLE
        article_recycler_view.adapter = ArticleAdapter(articles)
    }

    private fun showError() {
        progress_circular.visibility = View.GONE
        //Todo decide how to display error
    }
}
