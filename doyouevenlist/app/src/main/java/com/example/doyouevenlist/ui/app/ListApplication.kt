package com.example.doyouevenlist.ui.app

import android.app.Application
import com.example.doyouevenlist.data.api.NewsApi
import com.example.doyouevenlist.data.api.NewsRepository
import com.example.doyouevenlist.ui.app.ListApplication.Companion.BASE_URL
import com.example.doyouevenlist.ui.list_activity.viewmodel.ArticlesViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ListApplication : Application() {

    companion object {
        internal const val BASE_URL = "https://newsapi.org/v2/"
    }

    override fun onCreate() {
        super.onCreate()
        startKoin {
            modules(articlesModule)
        }
    }
}

val articlesModule = module {
    single { provideNewsApi(get()) }
    single { provideRetrofit() }
    single { NewsRepository(get()) }
    viewModel { ArticlesViewModel(get()) }
}

fun provideRetrofit() : Retrofit {
    return Retrofit.Builder().baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create()).build()
}

fun provideNewsApi(retrofit: Retrofit) : NewsApi = retrofit.create(NewsApi::class.java)