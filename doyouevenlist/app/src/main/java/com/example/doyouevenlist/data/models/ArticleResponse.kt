package com.example.doyouevenlist.data.models

//Todo make this generic
data class ArticleResponse(
    val status : String?,
    val totalResults : Int,
    val articles : List<Article> = listOf()
)