package com.example.doyouevenlist.data.api

import com.example.doyouevenlist.data.models.ArticleResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApi {

    @GET("everything")
    suspend fun getArticles(
        @Query("q") newsTopic : String,
        @Query("from") dateBeginSearch : String,
        @Query("sortBy") sort : String,
        @Query("apiKey") apiKey : String //Todo consider putting this in headers if time allows
    ) : ArticleResponse
}