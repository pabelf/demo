package com.example.doyouevenlist

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.doyouevenlist.data.api.NewsRepository
import com.example.doyouevenlist.data.models.Article
import com.example.doyouevenlist.data.models.Response
import com.example.doyouevenlist.data.models.Source
import com.example.doyouevenlist.ui.list_activity.viewmodel.ArticlesViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ArticlesViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var newsApiTD: NewsApiTD

    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        Dispatchers.setMain(Dispatchers.Unconfined)
        newsApiTD = NewsApiTD()
    }

    @ExperimentalCoroutinesApi
    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun fetching_articles_sets_loading_true() {
        newsApiTD.setArticlesToReturn(generateArticleList(2))
        val sut = ArticlesViewModel(NewsRepository(newsApiTD))

        sut.getArticles().observeForever {
            assert(it == Response.Loading)
        }
    }

    @Test
    fun fetching_articles_list_of_10_items_expect_list_of_10_items_returned() {
        newsApiTD.setArticlesToReturn(generateArticleList(10))
        val sut = ArticlesViewModel(NewsRepository(newsApiTD))

        sut.getArticles().observeForever {
            assert((it as Response.Success).data.status == "200" && it.data.totalResults == 10)
        }
    }

    @Test
    fun fetching_articles_exception_occurs_error_returned() {
        newsApiTD.setArticlesToReturn(generateArticleList(10))
        newsApiTD.setIsRequestSuccessful(false)
        val sut = ArticlesViewModel(NewsRepository(newsApiTD))

        sut.getArticles().observeForever {
            assert((it as Response.Error).exception.message == NewsApiTD.ERROR_MESSAGE)
        }
    }

    private fun generateArticleList(numberOfArticles: Int): List<Article> {
        val listToReturn: ArrayList<Article> = arrayListOf()
        for (i in 0 until numberOfArticles) {
            listToReturn.add(
                Article(
                    Source("", "SomeNewsSourceName"),
                    "",
                    "This is test title",
                    "This is test description"
                )
            )
        }

        return listToReturn
    }
}
