package com.example.doyouevenlist

import com.example.doyouevenlist.data.api.NewsApi
import com.example.doyouevenlist.data.models.Article
import com.example.doyouevenlist.data.models.ArticleResponse

class NewsApiTD : NewsApi {

    companion object {
        const val ERROR_MESSAGE = "something went wrong"
    }

    var articles: List<Article> = listOf()
    var isRequestSucessful: Boolean = true

    override suspend fun getArticles(
        newsTopic: String,
        dateBeginSearch: String,
        sort: String,
        apiKey: String
    ): ArticleResponse {
        if (isRequestSucessful) {
            return ArticleResponse("200", articles.size, articles)
        } else {
            throw Exception(ERROR_MESSAGE)
        }
    }

    fun setArticlesToReturn(articles: List<Article>) {
        this.articles = articles
    }

    fun setIsRequestSuccessful(isRequestSucessful: Boolean) {
        this.isRequestSucessful = isRequestSucessful
    }

}